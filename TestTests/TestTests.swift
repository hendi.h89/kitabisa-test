//
//  TestTests.swift
//  TestTests
//
//  Created by Hendi on 15/03/19.
//  Copyright © 2019 hendi's company. All rights reserved.
//

import XCTest
@testable import Test

class TestTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSum(){
        let viewModel = CalculatorViewModel()
        viewModel.sum("2", "2")
        XCTAssertEqual(viewModel.result, "4")
    }
    
    func testMultiply(){
        let viewModel = CalculatorViewModel()
        viewModel.multiply("2","3")
        XCTAssertEqual(viewModel.result, "6")
    }
    
    func testNPrime(){
        let viewModel = CalculatorViewModel()
        viewModel.nPrime("4")
        XCTAssertEqual(viewModel.result, "2, 3, 5, 7")
    }
    
    func testNFibonacci(){
        let viewModel = CalculatorViewModel()
        viewModel.nFibonacci("4")
        XCTAssertEqual(viewModel.result, "0, 1, 1, 2")
    }    
}
