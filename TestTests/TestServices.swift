//
//  TestServices.swift
//  TestTests
//
//  Created by Hendi on 15/03/19.
//  Copyright © 2019 hendi's company. All rights reserved.
//

import XCTest
@testable import Test

class TestServices: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSum(){
        XCTAssertEqual(CalculatorService.sum(x: 1, y: 2), 3)
    }
    
    func testMultiply(){
        XCTAssertEqual(CalculatorService.multiply(x: 1, y: 2), 2)
    }
    
    func testNPrime(){
        let result = [2,3,5,7]
        XCTAssertEqual(CalculatorService.nPrime(n: 4), result)
    }
    
    func testNFibonacci(){
        let result = [0,1,1,2]
        XCTAssertEqual(CalculatorService.nFibonacci(n: 4), result)
    }
    
    func testIsPrime(){
        XCTAssertTrue(CalculatorService.isPrime(number: 2))
    }
    
    func testFibonacci(){        
        XCTAssertEqual(CalculatorService.fibonacci(index: 3), 2)
    }
}
