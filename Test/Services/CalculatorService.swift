//
//  CalculatorService.swift
//  Test
//
//  Created by Hendi on 15/03/19.
//  Copyright © 2019 hendi's company. All rights reserved.
//

import Foundation

class CalculatorService {
    static func sum(x:Int, y:Int) -> Int {
        return (x+y)
    }
    
    static func multiply(x:Int, y:Int) -> Int {
        return (x*y)
    }
    
    static func nPrime(n:Int) -> Array<Int> {
        var data = Array<Int>()
        var prime = 2
        while(data.count != n){
            if isPrime(number: prime){
                data.append(prime)
            }
            prime += 1
        }
        return data
    }
    
    static func isPrime(number: Int) -> Bool {
        if number < 2 {
            return false
        }
        for i in 2..<number {
            if number % i == 0 {
                return false
            }
        }
        return true
    }
    
    static func nFibonacci(n:Int) -> Array<Int> {
        var data = Array<Int>()
        for i in 0..<n {
            data.append(fibonacci(index: i))
        }
        return data
    }
    
    static func fibonacci(index: Int) -> Int {
        guard index != 0, index != 1 else { return index }
        return fibonacci(index: index - 1) + fibonacci(index: index - 2)
    }  
}
