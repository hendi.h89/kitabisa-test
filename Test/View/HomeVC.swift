//
//  ViewController.swift
//  Test
//
//  Created by Hendi on 15/03/19.
//  Copyright © 2019 hendi's company. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btnSum: UIButton!
    @IBOutlet weak var btnMultiply: UIButton!
    @IBOutlet weak var btnNPrime: UIButton!
    @IBOutlet weak var btnNFibonacci: UIButton!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews(){
        self.title = "SIMPLE CALCULATOR"
        setupButton(btnSum)
        setupButton(btnMultiply)
        setupButton(btnNPrime)
        setupButton(btnNFibonacci)
    }
    
    func setupButton(_ button: UIButton){
        button.setTitleColor(UIColor.blue, for: .normal)
        button.layer.borderColor = UIColor.blue.cgColor
        button.layer.borderWidth = 2
        button.layer.cornerRadius = button.bounds.height / 4
    }

    @IBAction func actionSum(_ sender: Any) {
        pushVC("sum")
    }
    
    @IBAction func actionMultiply(_ sender: Any) {
        pushVC("multiply")
    }
    
    @IBAction func actionNPrime(_ sender: Any) {
        pushVC("n_prime")
    }
    
    @IBAction func actionFibonacci(_ sender: Any) {
        pushVC("n_fibonacci")
    }
    
    func pushVC(_ id: String){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: id)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

