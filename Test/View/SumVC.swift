//
//  SumVC.swift
//  Test
//
//  Created by Hendi on 15/03/19.
//  Copyright © 2019 hendi's company. All rights reserved.
//

import UIKit

class SumVC: UIViewController {
    
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var tfX: UITextField!
    @IBOutlet weak var tfY: UITextField!
    
    var viewModel: CalculatorViewModel! {
        didSet{
            lblResult.text = viewModel.result
            lblResult.textColor = viewModel.resultLabelColor
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "X + Y = RESULT"
        self.viewModel = CalculatorViewModel()
    }
 
    @IBAction func onXChanged(_ sender: UITextField) {
        showResult()
    }
    
    @IBAction func onYChanged(_ sender: UITextField) {
        showResult()
    }
    
    func showResult(){
        viewModel.sum(tfX.text, tfY.text)
        lblResult.text = viewModel.result
        lblResult.textColor = viewModel.resultLabelColor
    }
}
