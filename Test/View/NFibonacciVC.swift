//
//  NFibonacciVC.swift
//  Test
//
//  Created by Hendi on 15/03/19.
//  Copyright © 2019 hendi's company. All rights reserved.
//

import UIKit

class NFibonacciVC: UIViewController {

    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var tfN: UITextField!
    
    var viewModel: CalculatorViewModel! {
        didSet{
            lblResult.text = viewModel.result
            lblResult.textColor = viewModel.resultLabelColor
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "(N) FIBONACCI = RESULT"
        self.viewModel = CalculatorViewModel()
    }
    
    @IBAction func onNChanged(_ sender: UITextField) {
        viewModel.nFibonacci(tfN.text)
        lblResult.text = viewModel.result
        lblResult.textColor = viewModel.resultLabelColor
    }  
}
