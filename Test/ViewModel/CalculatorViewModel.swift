//
//  CalculatorViewModel.swift
//  Test
//
//  Created by Hendi on 15/03/19.
//  Copyright © 2019 hendi's company. All rights reserved.
//

import Foundation
import UIKit

class CalculatorViewModel {
    
    var result = ""
    var resultLabelColor = UIColor.red
    
    func sum(_ x:String?, _ y:String?){
        self.resultLabelColor = UIColor.red
        guard let _x = x, !_x.isEmpty else {
            self.result = "X can not be empty"
            return
        }
        guard let xInt = Int(_x) else {
            self.result = "X is not number"
            return
        }
        
        guard let _y = y, !_y.isEmpty else {
            self.result = "Y can not be empty"
            return
        }
        guard let yInt = Int(_y) else {
            self.result = "Y is not number"
            return
        }
        
        // if pass
        self.resultLabelColor = UIColor.blue
        self.result = "\(CalculatorService.sum(x: xInt, y: yInt))"
    }
    
    func multiply(_ x:String?, _ y:String?){
        self.resultLabelColor = UIColor.red
        guard let _x = x, !_x.isEmpty else {
            self.result = "X can not be empty"
            return
        }
        guard let xInt = Int(_x) else {
            self.result = "X is not number"
            return
        }
        
        guard let _y = y, !_y.isEmpty else {
            self.result = "Y can not be empty"
            return
        }
        guard let yInt = Int(_y) else {
            self.result = "Y is not number"
            return
        }
        
        // if pass
        self.resultLabelColor = UIColor.blue
        self.result = "\(CalculatorService.multiply(x: xInt, y: yInt))"
    }
    
    func nPrime(_ n:String?){
        self.resultLabelColor = UIColor.red
        guard let _n = n, !_n.isEmpty else {
            self.result = "N can not be empty"
            return
        }
        guard let nInt = Int(_n) else {
            self.result = "N is not number"
            return
        }
        
        // if pass
        self.resultLabelColor = UIColor.blue
        self.result = "\(CalculatorService.nPrime(n: nInt))".replacingOccurrences(of: "]", with: "").replacingOccurrences(of: "[", with: "")
    }
    
    func nFibonacci(_ n:String?){
        self.resultLabelColor = UIColor.red
        guard let _n = n, !_n.isEmpty else {
            self.result = "N can not be empty"
            return
        }
        guard let nInt = Int(_n) else {
            self.result = "N is not number"
            return
        }
        
        // if pass
        self.resultLabelColor = UIColor.blue
        self.result = "\(CalculatorService.nFibonacci(n: nInt))".replacingOccurrences(of: "]", with: "").replacingOccurrences(of: "[", with: "")
    }
}
